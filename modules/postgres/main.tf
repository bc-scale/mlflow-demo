terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.6.0"
    }
  }
}

resource "helm_release" "postgres" {
  name       = var.name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"
  version    = "11.8.0"

  set {
    name  = "global.postgresql.auth.username"
    value = var.username
  }

  set_sensitive {
    name  = "global.postgresql.auth.password"
    value = var.password
  }

  set {
    name  = "global.postgresql.auth.database"
    value = var.database
  }

  set {
    name  = "global.postgresql.service.ports.postgresql"
    value = var.port
  }
}

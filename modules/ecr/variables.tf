variable "aws_region" {
  type = string
}

variable "aws_account_role_arn" {
  type = string
}

terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.6.0"
    }
  }
}

resource "helm_release" "minio" {
  name       = var.name
  repository = "https://charts.min.io/"
  chart      = "minio"
  version    = "4.0.2"

  set {
    name  = "resources.requests.memory"
    value = "2Gi"
  }

  set {
    name  = "mode"
    value = "standalone"
  }

  set {
    name  = "minioAPIPort"
    value = var.api_port
  }

  set {
    name  = "users[0].accessKey"
    value = var.access_key
  }

  set_sensitive {
    name  = "users[0].secretKey"
    value = var.secret_key
  }

  set {
    name  = "users[0].policy"
    value = "consoleAdmin"
  }

  set {
    name  = "buckets[0].name"
    value = var.default_bucket_name
  }

  set {
    name  = "buckets[0].policy"
    value = "none"
  }

  set {
    name  = "buckets[0].purge"
    value = false
  }
}

variable "chart_repository_uri" {
  type = string
}

variable "chart_repository_username" {
  type = string
}

variable "chart_repository_password" {
  type      = string
  sensitive = true
}

variable "image_repository_aws_region" {
  type = string
}

variable "image_repository_aws_account_role_arn" {
  type = string
}

variable "postgres_username" {
  type = string
}

variable "postgres_password" {
  type      = string
  sensitive = true
}

variable "postgres_database" {
  type = string
}

variable "storage_access_key" {
  type = string
}

variable "storage_secret_key" {
  type      = string
  sensitive = true
}

variable "name" {
  type = string
}

variable "access_key" {
  type = string
}

variable "secret_key" {
  type      = string
  sensitive = true

  validation {
    condition     = length(var.secret_key) >= 8 && length(var.secret_key) <= 40
    error_message = "The secret key is invalid. (secret key length should be between 8 and 40)."
  }
}

variable "api_port" {
  type = number
}

variable "default_bucket_name" {
  type = string
}
